package org.tuxjitl.employeemanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tuxjitl.employeemanager.exception.UserNotFoundException;
import org.tuxjitl.employeemanager.model.Employee;
import org.tuxjitl.employeemanager.repository.EmployeeRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{

    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {

        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee addEmployee(Employee employee) {

        employee.setEmployeeCode(UUID.randomUUID().toString());

        employeeRepository.save(employee);

        return null;
    }

    @Override
    public List<Employee> retrieveAllEmployees() {

        return employeeRepository.findAll();
    }

    @Override
    public Employee updateEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public void deleteEmployeeById(Long id) {
        employeeRepository.deleteEmployeeById(id);
    }

    @Override
    public Employee retrieveEmployeeById(Long id) {

        return employeeRepository.findEmployeeById(id)
                .orElseThrow(()-> new UserNotFoundException("User with id: " + id + " was not found"));
    }


}
