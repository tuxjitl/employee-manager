package org.tuxjitl.employeemanager.service;

import org.tuxjitl.employeemanager.model.Employee;

import java.util.List;

public interface EmployeeService {

    Employee addEmployee(Employee employee);
    List<Employee> retrieveAllEmployees();
    Employee updateEmployee(Employee employee);
    void deleteEmployeeById(Long id);
    Employee retrieveEmployeeById(Long id);

}
